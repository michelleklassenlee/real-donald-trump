# coding: utf-8

# ## Will the Real Donald Trump please stand up?
# 
# This jupyter notebook analyzes the Tweets coming from the @RealDonaldTrump account from the inception in 2012 through 25 April. It demonstrates that bag-of-words vectorization with a Linear Support Vector Machine performs very well in predicting when the account is tweeting from an Android device, simply on the text of a Tweet. 
# 
# This demonstrates that as @RealDonaldTrump transitioned away from Android use in March and April 2017, it is still likely that he is still tweeting, just from an iPhone. 

# In[1]:

import numpy as np
from sklearn.svm import SVC
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import re

get_ipython().magic('matplotlib inline')


# ### Read data in
# 
# Thanks http://www.trumptwitterarchive.com/
# Before run, scrape to raw_tweets.txt.
# 
# Parse out thee datetime, device, and tweet. Read into a dataframe. Remove URLs.

# In[2]:

#read the data from raw files into a data frame
def readIn(file):
    # Create regex to parse out date, device tag, and tweet text
    datetime_regex = '[A-z]{3} [0-9]{1,2}, 20[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} [A-Z]{2}'
    device_regex = '\[([A-Za-z0-9 _\(\)\-]*)\]' #[A-Za-z0-9 _\(\)]
    tweet_regex = '[0-9]{2}:[0-9]{2}:[0-9]{2} [A-Z]{2}(.*)\['
    # these will be the headers for the pandas data frames
    h = ["date","tweet","device"] 
    file_name = file
    newFile = open (file_name, "rb")
    f = newFile.read()
    newFile.close()
    out=[]
    # the lines in the file are seperated by '\r\n' and the first couple of lines are header text
    for i in f.decode('utf-8').split('\r\n')[12:]:
        dt = re.search(datetime_regex,i).group(0)
        dev = re.findall(device_regex,i)[-1]       
        tw = re.search(tweet_regex,i).group(1)
        out.append([dt,tw,dev])
    df = pd.DataFrame(out,columns=h)
    return df


# In[3]:

df = readIn('raw_tweets.txt')
df['datetime'] = pd.to_datetime(df['date'],format= '%b %d, %Y %I:%M:%S %p')
df = df.set_index(df['datetime'])
df['day'] = df.index.round('D')  
df['Android'] = np.where(df.device.str.contains('Android') ,1, 0)
df['tweet'] = df['tweet'].str.replace('http[-a-zA-Z0-9@:%._\+~#=/]+','URL')


# ### Let's analyze the tweets a bit

# The first and last time he used an Android.

# In[4]:

fsls = df.groupby(['Android']).datetime.agg({'date ': ['first', 'last']})
fsls


# This is when he was phasing out the Android.

# In[5]:

df2 = df.head(270).groupby(['day','Android'])['day'].count().unstack('Android').fillna(0)
df2[[0,1]].plot(kind='bar', stacked=True)


# Frequency of tweet by day.

# In[6]:

plt.plot(df.resample('D').count())


# ### Prep the data for training.
# 
# - Remove retweets
# - Remove Twitter Web Client tweets (too ambiguous)
# - Remove all the numbers from tweets

# In[7]:

#take out all of the retweets
df = df[~df['tweet'].str.contains('\"@[A-Za-z0-9_]+: ')]
df = df[~df['tweet'].str.contains('RT @[A-Za-z0-9_]+: ')]
# Take out the Twitter Web Client tweets out
df = df[~df['device'].str.contains('Twitter Web Client')]
#remove all of the numbers from the tweets
df['tweet'] = df['tweet'].str.replace('(\d+)','')


# ### Create an 80/20 training set
# Only keep data before 25 February 2017 (the period when he is using an Android)

# In[8]:

train_percent =  0.8 
dft = df.ix[datetime.date(year=2017,month=2,day=25):]
print(len(dft))
msk = np.random.rand(len(dft))< train_percent
train = dft[msk]
test = dft[~msk]


# Devices used in this time period

# In[9]:

dft.groupby('device').count()


# In[10]:

percent_android = len(dft[dft['device']=='Twitter for Android'])/len(dft)
print(percent_android)


# ### Vecorize 
# - Simple boolean 
# - Keep case (DT likes to yell)

# In[11]:

count_vectorizer = CountVectorizer( lowercase=False)
counts = count_vectorizer.fit_transform(train['tweet'].values)


# In[12]:

print(count_vectorizer.get_feature_names()[:10])


# ### Label

# In[13]:

label =  np.where(train.device.str.contains('Android') ,1, 0)


# ### Train the model

# In[14]:

# use a vanilla SVM
svm = SVC(kernel='linear',probability=True)
svm.fit(counts, label)


# ### Test the model

# In[15]:

# we use the same vectorizor here so the indexing of the vocab is the same
test_counts = count_vectorizer.transform(test['tweet'].values)
print('The 1000th index for the testing set corresponds to: '+str(count_vectorizer.get_feature_names()[1000]))

test_label = np.where(test.device.str.contains('Android') ,1,0)
#test the svm model on the test data
predictions = svm.predict(test_counts)
pred_prob = svm.predict_proba(test_counts)


# ### Performance

# In[16]:

confusion = confusion_matrix(test_label, predictions)

recall = confusion[1][1]/(confusion[1][1]+confusion[0][1])
percision = confusion[1][1]/(confusion[1][1]+confusion[1][0])
score =(2*percision*recall)/(percision+recall)


print('The confusion matrix associated with the SVM model is: '+str(confusion))
print('The percision associated with the SVM model is: '+str(percision))
print('The recall associated with the SVM model is: '+str(recall))
print('The F score associated with the SVM model is: '+str(score))


# ### Discriminating words

# In[17]:

coef = svm.coef_.data
top_features = 40
feature_names = count_vectorizer.get_feature_names()
top_positive_coefficients = np.argsort(coef)[-top_features:]
top_negative_coefficients = np.argsort(coef)[:top_features]
top_coefficients = np.hstack([top_negative_coefficients, top_positive_coefficients])
plt.figure(figsize=(15, 5))
colors = ['red' if c < 0 else 'blue' for c in coef[top_coefficients]]
plt.bar(np.arange(2 * top_features), coef[top_coefficients], color=colors)
feature_names = np.array(feature_names)
plt.xticks(np.arange(1, 1 + 2 * top_features), feature_names[top_coefficients], rotation=60, ha='right')
plt.show()


# ### Example perdictions -- Sanity Check

# In[18]:

test['predictions'] = predictions
test['pred_prob'] = list(pred_prob)
test


# ### Actual device use

# In[19]:

df2 = test.groupby(['day','Android'])['day'].count().unstack('Android').fillna(0)

df2[[0,1]].tail(50).plot(kind='bar', stacked=True)


# ### Predicted device use

# In[20]:

df2 = test.groupby(['day','predictions'])['day'].count().unstack('predictions').fillna(0)

df2[[0,1]].tail(50).plot(kind='bar', stacked=True)


# ### Let's perdict the future!!
# 
# Run the trained model on the data after he stoped using an Android, from 25 February 2017 to 25 April 2017

# In[21]:

test = df.ix[:datetime.date(year=2017,month=2,day=25)]
len(test)


# In[22]:

# we use the same vectorizor here so the indexing of the vocab is the same
test_counts = count_vectorizer.transform(test['tweet'].values)
print('The 1000th index for the testing set corresponds to: '+str(count_vectorizer.get_feature_names()[1000]))

test_label = np.where(test.device.str.contains('Android') ,1,0)
#test the svm model on the test data
predictions = svm.predict(test_counts)
test['predictions'] = predictions


# ### The data as tagged by Android v. Non-Android on twitter by day

# In[23]:

df2 = test.groupby(['day','Android'])['day'].count().unstack('Android').fillna(0)
df2
df2[[0,1]].plot(kind='bar', stacked=True)


# ### Predicted device use 
# 
# Even though he essentially stopped using an Android after February 2017, his Android-like voice lives on!

# In[24]:

df2 = test.groupby(['day','predictions'])['day'].count().unstack('predictions').fillna(0)
df2
df2[[0,1]].plot(kind='bar', stacked=True)


# In[25]:

test

