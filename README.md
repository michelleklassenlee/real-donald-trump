This is meant to be run in a Jupyter Python 3 environment. 

I have included a python script as well, but this will not print well in a shell.

All the data is pre-downloaded in a text file 'raw_tweets.txt' with data from
25 April 2017 from http://www.trumptwitterarchive.com/.
